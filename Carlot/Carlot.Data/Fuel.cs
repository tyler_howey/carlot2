﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carlot.Data
{
    public enum Fuel
    {
        Gas,
        Diesel,
        Methane
    }
}
