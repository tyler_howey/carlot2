﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carlot.Data
{
    public static class Lot
    {
        private static List<Car> _cars = new List<Car>();

        public static void AddCar()
        {
            Console.Clear();
            Console.WriteLine("Please enter name of the car: ");
            string carName = Console.ReadLine();
            Console.Clear();

            int carChoice = 0;
            foreach(var car in Enum.GetValues(typeof(CarType)))
            {
                Console.WriteLine((int)car + ":" + car.ToString());
            }
            Console.WriteLine("Please enter car choice:");
            carChoice = int.Parse(Console.ReadLine());

            Fuel fuelType;
            int cost = 20000;
            int fuelLevel = 14;
            if(carChoice == (int)CarType.Pinto)
            {
                fuelType = Fuel.Gas;
            }
            else if(carChoice == (int)CarType.Semi)
            {
                fuelType = Fuel.Diesel;
                cost = 50000;
                fuelLevel = 50;
            }
            else
            {
                fuelType = Fuel.Methane;
            }

            _cars.Add(new Car(carName, (CarType)carChoice, fuelType, cost, fuelLevel));
        }

        public static void DriveCar()
        {
            int x = 0;
            foreach(Car c in _cars)
            {
                Console.WriteLine(x + ":" + c.GetName());
                x++;
            }
            Console.WriteLine("Please select car:");
            int choice = int.Parse(Console.ReadLine());

            Car selected = _cars[choice];

            if(selected.GetFuelLevel() < 3 || (selected.GetCarType() == CarType.Funny && selected.GetFuelLevel() < 14))
            {
                Console.WriteLine("Oh No, You are out of fuel");
                Console.ReadKey();
                return;
            }
            else
            {
                selected.ReduceFuel();
                Console.WriteLine("Zoom");
                Console.ReadKey();
            }
            
        }

        public static void Honk()
        {
            Console.Clear();
            Console.WriteLine("Beep");
        }

        public static void FuelUp()
        {
            Console.Clear();

            int x = 0;
            foreach (Car c in _cars)
            {
                Console.WriteLine(x + ":" + c.GetName());
                x++;
            }
            Console.WriteLine("Please select car:");
            int choice = int.Parse(Console.ReadLine());

            Car selected = _cars[choice];
            Console.Clear();

            Console.WriteLine("Please enter amount of fuel:");
            int fuel = int.Parse(Console.ReadLine());

            foreach (var type in Enum.GetValues(typeof(Fuel)))
            {
                Console.WriteLine((int)type + ":" + type.ToString());
            }
            Console.WriteLine("Please enter fuel type:");
            int fType = int.Parse(Console.ReadLine());

            if((Fuel)fType != selected.GetFuelType())
            {
                selected.NotDriveable();
                Console.WriteLine("You have used the worng fuel and the car will no longer start.");
                Console.ReadKey();
                return;
            }
            else
            {
                selected.AddFuel(fuel);
            }
        }
    }
}
