﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carlot.Data
{
    class Car
    {
        string CarName { get; set; }
        CarType Type { get; set; }
        Fuel FuelType { get; set; }
        int Cost { get; set; }
        int FuelLevel { get; set; }
        bool Driveable { get; set; }

        public Car(string name, CarType car, Fuel fType, int cost, int level)
        {
            CarName = name;
            Type = car;
            FuelType = fType;
            Cost = cost;
            FuelLevel = level;
            Driveable = true;
        }

        public string GetName()
        {
            return CarName;
        }

        public int GetFuelLevel()
        {
            return FuelLevel;
        }

        public CarType GetCarType()
        {
            return this.Type;
        }

        public Fuel GetFuelType()
        {
            return FuelType;
        }

        public void ReduceFuel()
        {
            if(this.Type == CarType.Funny)
            {
                FuelLevel -= 14;
            }
            else
            {
                FuelLevel -= 3;
            }
        }

        public void AddFuel(int amount)
        {
            FuelLevel += amount;
        }

        public void NotDriveable()
        {
            Driveable = false;
        }
    }
}
