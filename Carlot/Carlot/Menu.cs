﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Carlot.Data;

namespace Carlot
{
    public static class Menu
    {
        public static void ShowMenu()
        {
            string choice;
            do
            {
                Console.WriteLine("1: Add Car");
                Console.WriteLine("2: Drive Car");
                Console.WriteLine("3: Honk");
                Console.WriteLine("4: Add fuel");

                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Lot.AddCar();
                        break;
                    case "2":
                        Lot.DriveCar();
                        break;
                    case "3":
                        Lot.Honk();
                        break;
                    case "4":
                        Lot.FuelUp();
                        break;
                }
                Console.Clear();
            } while (choice != "10");
            
        }
    }
}
